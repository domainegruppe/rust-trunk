ARG RUST_VERSION=${RUST_VERSION:-latest}

FROM rust:$RUST_VERSION

RUN rustup target add wasm32-unknown-unknown
RUN cargo install --locked trunk