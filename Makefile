.PHONY: build

build:
	@read -p "Enter Rust Version: " RUST_VERSION; \
	docker build --label "RUST_VERSION=$$RUST_VERSION" --build-arg RUST_VERSION=$$RUST_VERSION -t gitlab.com/domainegruppe/rust-trunk:$$RUST_VERSION -t gitlab.com/domainegruppe/rust-trunk:latest .
	docker push gitlab.com/domainegruppe/rust-trunk:$$RUST_VERSION
